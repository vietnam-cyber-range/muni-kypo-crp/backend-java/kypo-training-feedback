package cz.muni.ics.kypo.training.feedback.repository;

import cz.muni.ics.kypo.training.feedback.model.SubGraph;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubGraphRepository extends JpaRepository<SubGraph, Long> {
}
